# renovate: datasource=docker depName=nginx
ARG NGINX_VERSION=1.23.4
FROM nginx:$NGINX_VERSION-alpine

COPY css /usr/share/nginx/html/css
COPY img /usr/share/nginx/html/img
COPY index.html /usr/share/nginx/html/
